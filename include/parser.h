#ifndef BOOST_LOG_DYN_LINK
#define BOOST_LOG_DYN_LINK 1

#endif

#ifndef PARSER_H
#define PARSER_H

#include "message.h"
#include "time_util.h"
#include <vector>
#include <string>
#include <fstream>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/common.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/core/null_deleter.hpp>

using namespace boost::log;

class parser
{
public:
	parser();
	~parser();
	void parse(std::vector<message>*, std::string);
private:
	std::ofstream log;
	typedef sinks::asynchronous_sink<sinks::text_ostream_backend> text_sink;
	boost::shared_ptr<text_sink> sink;
	sources::logger lg;
};

#endif