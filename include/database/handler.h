#ifndef HANDLER_H
#define HANDLER_H

#include <pqxx/pqxx>

using namespace pqxx;

class handler
{
public:
	handler();
	~handler();
	result handle(connection&, std::string);
};

#endif