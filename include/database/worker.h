#ifndef WORKER_H
#define WORKER_H

#include <string>
#include <iostream>
#include <pqxx/pqxx>
#include <boost/shared_ptr.hpp>

using namespace pqxx;

class worker
{
public:
	worker(std::string);
	~worker();
	connection conn;
};

#endif