#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/gregorian/greg_month.hpp>

using namespace boost::gregorian;

class calculator
{
public:
	calculator();
	~calculator();

	/**
	 * \brief calculate_interest
	 * \details mengembalikan interest berdasarkan tanggal mulai deposito, tanggal cair deposito, tanggal hari ini,
	 * dan jumlah awal deposito serta suku bunganya (terpotong pajak 20%)
	 * 
	 * \param initial_amount amount awal yang ditaruh di bank sebagai deposito
	 * \param interest_rate suku bunga yang ditawarkan dari bank
	 * \param start_date tanggal mulai menaruh deposito
	 * \param due_date tanggal deposito cair
	 * \param today tanggal hari ini
	 * \return hasil bunga yang didapatkan sesudah dipotong pajak
	 */
	long long calculate_interest(long long initial_amount, long double interest_rate,
		date start_date, date due_date, date today);
private:
	const long double net = 0.8; // tax = 0.2
};

#endif