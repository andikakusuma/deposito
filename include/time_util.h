#ifndef TIME_UTIL_H
#define TIME_UTIL_H

#include <boost/date_time/posix_time/posix_time.hpp>

std::string addZero(int);

std::string getTime();

#endif