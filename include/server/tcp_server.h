#ifndef BOOST_LOG_DYN_LINK
#define BOOST_LOG_DYN_LINK 1

#endif

#ifndef TCP_SERVER_H
#define TCP_SERVER_H

#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/array.hpp>
#include <boost/shared_ptr.hpp>
#include "tcp_connection.h"
#include "../parser.h"
#include "../database/worker.h"
#include "../time_util.h"
#include "../calculator.h"
#include <pqxx/pqxx>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/common.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/core/null_deleter.hpp>

using boost::asio::ip::tcp;
using namespace boost::log;

class tcp_server
{
public:
	// constructor
	tcp_server(boost::asio::io_service& io);
private:
	tcp::acceptor acceptor;
	boost::shared_ptr<worker> worker_;
	calculator calculator_;
	handler handler_;
	parser parser_;
	std::ofstream log;
	typedef sinks::asynchronous_sink<sinks::text_ostream_backend> text_sink;
	boost::shared_ptr<text_sink> sink;
	sources::logger lg;
	// membuat socket dan memulai async accept
	void start_accept();
	// menghandle request client lalu menginisialisasi operasi accept berikutnya
	void handle_accept(tcp_connection::pointer new_connection,
		const boost::system::error_code& error);
};

#endif
