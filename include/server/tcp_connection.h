#ifndef TCP_CONNECTION_H
#define TCP_CONNECTION_H

#include <iostream>
#include <string>
#include <ctime>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/array.hpp>
#include "../database/handler.h"

using boost::asio::ip::tcp;

class tcp_connection
	: public boost::enable_shared_from_this<tcp_connection>
{
public:
	typedef boost::shared_ptr<tcp_connection> pointer;
	static pointer create(boost::asio::io_service&);
	tcp::socket& socket();
	void start();
private:
	tcp_connection(boost::asio::io_service&);
	void handle_write();
	tcp::socket socket_;
	std::string message;
};

#endif