#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/gregorian/greg_month.hpp>

class message
{
public:
	message(std::string, std::string, boost::posix_time::ptime,
		std::string, int, boost::gregorian::date, boost::gregorian::date,
		double, int);
	~message();
	std::string get_product();
	std::string get_userprocess();
	boost::posix_time::ptime get_date_in();
	std::string get_placement_bank();
	long long get_initial_amount();
	boost::gregorian::date get_start_date();
	boost::gregorian::date get_due_date();
	long double get_interest_rate();
	long long get_withdrawal_money();
private:
	std::string product;
	std::string userprocess;
	boost::posix_time::ptime date_in;
	std::string placement_bank;
	long long initial_amount;
	boost::gregorian::date start_date;
	boost::gregorian::date due_date;
	long double interest_rate;
	long long withdrawal_money;
};

#endif