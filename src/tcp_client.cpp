#ifndef BOOST_LOG_DYN_LINK
#define BOOST_LOG_DYN_LINK 1

#endif

#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/common.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/sources/logger.hpp>
#include <boost/core/null_deleter.hpp>
#include <iostream>
#include <ostream>
#include <istream>
#include <fstream>
#include "../include/time_util.h"

using boost::asio::ip::tcp;
using namespace boost::log;

typedef sinks::asynchronous_sink<sinks::text_ostream_backend> text_sink;

int main(int argc, char** argv)
{
	boost::shared_ptr<text_sink> sink = boost::make_shared<text_sink>();

	std::ofstream log;

	boost::posix_time::ptime date_time = boost::posix_time::microsec_clock::universal_time();
	std::string date_now = std::to_string(date_time.date().year()) + '_' +
		addZero(date_time.date().month().as_number()) + '_' +
		addZero(date_time.date().day());
	std::cout << date_now << std::endl;
	log.open("../log/client_" + date_now + ".log", std::ios::out | std::ios::app );
	boost::shared_ptr<std::ostream> stream{&log, boost::null_deleter{}};
	sink->locked_backend()->add_stream(stream);

	core::get()->add_sink(sink);

	sources::logger lg;
	try
	{
		if(argc != 3)
		{
			std::cerr << "usage :client host port" << std::endl;
			return 1;
		}

		// requires io service
		boost::asio::io_service io;

		// get a list of endpoints
		tcp::resolver resolver(io);
		tcp::resolver::query query(argv[1], argv[2]);
		tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

		std::cout << "establishing connection" << std::endl;

		BOOST_LOG(lg) << getTime() << '|' << "tcpclient" << '|' << "1 >> " << "Establishing connection";
		sink->flush();

		// try each endpoint until successfully established connection
		tcp::socket socket(io);
		boost::asio::connect(socket, endpoint_iterator);

		std::cout << "connection success" << std::endl;
		
		BOOST_LOG(lg) << getTime() << '|' << "tcpclient" << '|' << "1 >> " << "Connection success";
		sink->flush();

		std::string input;

		while (true)
		{
			std::cin >> input;

			if(input == "exit")
			{
				std::cout << "Exited" << std::endl;
				break;
			}

			if(input == "start")
			{

				std::string input_file;
				std::cin >> input_file;

				std::ifstream in;
				in.open("../" + input_file);

				if(in.is_open())
				{
					boost::asio::connect(socket, endpoint_iterator);

					BOOST_LOG(lg) << getTime() << '|' << "tcpclient" << '|' << "1 >> " << "Sending request from file " << input_file;
					sink->flush();

					std::string input_request;
					getline(in, input_request, '\0');
					std::cout << input_request << std::endl;

				    boost::asio::streambuf request;
	  				std::ostream request_stream(&request);

					boost::posix_time::ptime date_time = boost::posix_time::microsec_clock::universal_time();

	  				request_stream << "this pc" << std::endl;
	  				request_stream << date_time << std::endl;
	  				//request_stream << boost::posix_time::to_simple_string(date_time) << std::endl;
				    request_stream << input_request << std::endl;

				    boost::asio::write(socket, request);
				}

			}
		}

	} catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}

	BOOST_LOG(lg) << getTime() << '|' << "tcpclient" << '|' << "1 >> " << "Exited";
	sink->flush();

	log.close();
	return 0;
}