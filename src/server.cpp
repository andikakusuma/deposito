#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/array.hpp>
#include "../include/server/tcp_server.h"

int main()
{
	try
	{
		boost::asio::io_service io;
		tcp_server server_tcp(io);

		io.run();
	} catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	return 0;
}