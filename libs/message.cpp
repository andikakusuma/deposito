#include "../include/message.h"

message::message(std::string product_, std::string userprocess_, boost::posix_time::ptime date_in_,
	std::string placement_bank_, int initial_amount_, boost::gregorian::date start_date_,
	boost::gregorian::date due_date_, double interest_rate_, int withdrawal_money_)
{
	product = product_;
	userprocess = userprocess_;
	date_in = date_in_;
	placement_bank = placement_bank_;
	initial_amount = initial_amount_;
	start_date = start_date_;
	due_date = due_date_;
	interest_rate = interest_rate_;
	withdrawal_money = withdrawal_money_;
}

message::~message()
{
}

std::string message::get_product()
{
	return product;
}
std::string message::get_userprocess()
{
	return userprocess;
}
boost::posix_time::ptime message::get_date_in()
{
	return date_in;
}
std::string message::get_placement_bank()
{
	return placement_bank;
}
long long message::get_initial_amount()
{
	return initial_amount;
}
boost::gregorian::date message::get_start_date()
{
	return start_date;
}
boost::gregorian::date message::get_due_date()
{
	return due_date;
}
long double message::get_interest_rate()
{
	return interest_rate;
}
long long message::get_withdrawal_money()
{
	return withdrawal_money;
}