#include "../../include/server/tcp_connection.h"

using boost::asio::ip::tcp;

typedef boost::shared_ptr<tcp_connection> pointer;

pointer tcp_connection::create(boost::asio::io_service& io)
{
	return pointer(new tcp_connection(io));
}

tcp::socket& tcp_connection::socket()
{
	return socket_;
}

void tcp_connection::start()
{
}

tcp_connection::tcp_connection(boost::asio::io_service& io) : socket_(io)
{
}

void tcp_connection::handle_write()
{
}