#ifndef BOOST_LOG_DYN_LINK
#define BOOST_LOG_DYN_LINK 1

#endif

#include "../../include/server/tcp_server.h"

using boost::property_tree::ptree;
using boost::asio::ip::tcp;
using namespace boost::log;
using namespace boost::gregorian;

typedef sinks::asynchronous_sink<sinks::text_ostream_backend> text_sink;

// constructor
tcp_server::tcp_server(boost::asio::io_service& io) : acceptor(io, tcp::endpoint(tcp::v4(), 1025))
{
	sink = boost::make_shared<text_sink>();

	boost::posix_time::ptime date_time = boost::posix_time::microsec_clock::universal_time();
	std::string date_now = std::to_string(date_time.date().year()) + '_' +
		addZero(date_time.date().month().as_number()) + '_' +
		addZero(date_time.date().day());
	log.open("../log/server_" + date_now + ".log", std::ios::out | std::ios::app );
	boost::shared_ptr<std::ostream> stream{&log, boost::null_deleter{}};
	sink->locked_backend()->add_stream(stream);

	core::get()->add_sink(sink);
	BOOST_LOG(lg) << getTime() << '|' << "tcp_server" << '|' << "1 >> " << "Server is running";
	sink->flush();

	worker_ = boost::shared_ptr<worker>(
		new worker("dbname=postgres user=postgres password=abcde12345 host=127.0.0.1 port=5432"));

	start_accept();
}

// membuat socket dan memulai async accept
void tcp_server::start_accept()
{
	tcp_connection::pointer new_connection = tcp_connection::create(acceptor.get_io_service());
	acceptor.async_accept(new_connection->socket(), boost::bind(&tcp_server::handle_accept,
		this, new_connection, boost::asio::placeholders::error));
}

// menghandle request client lalu menginisialisasi operasi accept berikutnya
void tcp_server::handle_accept(tcp_connection::pointer new_connection,
	const boost::system::error_code& error)
{
	try
	{
		if((worker_->conn).is_open() && !error)
		{
			// membaca request dari client
			boost::array<char, 128> buf;
			size_t len = new_connection->socket().read_some(boost::asio::buffer(buf));
			std::string input = buf.data();

			BOOST_LOG(lg) << getTime() << '|' << "tcp_server" << '|' << "1 >> " << "Parsing request";
			sink->flush();

			std::vector<message> list_of_message;
			parser_.parse(&list_of_message, input);

			for(size_t i = 0; i < list_of_message.size(); i++)
			{
				// tambahin ke database deposito yang baru
				std::stringstream ss;
				ss << "insert into position(id, product, userprocess, date_in, placement_bank, ";
				ss << "initial_amount, start_date, due_date, interest_rate, withdrawal_money) values(default, '";
				ss << list_of_message[i].get_product()  << "', '" << list_of_message[i].get_userprocess() << "', '";
				ss << list_of_message[i].get_date_in() << "', '" << list_of_message[i].get_placement_bank() << "', '";
				ss << list_of_message[i].get_initial_amount() << "', '" << list_of_message[i].get_start_date() << "', '";
				ss << list_of_message[i].get_due_date() << "', '" << list_of_message[i].get_interest_rate() << "', '";
				ss << list_of_message[i].get_withdrawal_money() << "');";

				handler_.handle(worker_->conn, ss.str());

				if (handler_.handle(worker_->conn, "SELECT * FROM SUMMARY WHERE PRODUCT = '" + 
					list_of_message[i].get_product() + "';").size() == 0)
				{
					// product belum ada di tabel summary
					handler_.handle(worker_->conn, "INSERT INTO SUMMARY VALUES(DEFAULT, '" + 
						list_of_message[i].get_product() + "', 0, 0, 0, 0, 0, 0);");
				}

				// kurangin cash total dari productnya
				handler_.handle(worker_->conn, "UPDATE SUMMARY SET TOTAL_CASH_TODAY = TOTAL_CASH_TODAY - " +
					std::to_string(list_of_message[i].get_initial_amount()) + " WHERE PRODUCT = '" +
					list_of_message[i].get_product() + "';");
			}

			// restart summary table
			ptree pt; // ptree untuk menyimpan data summary sementara sebelum di update ke database summary
			result sum_res = handler_.handle(worker_->conn, "SELECT * FROM SUMMARY;");
			for (result::const_iterator c = sum_res.begin(); c != sum_res.end(); ++c)
			{
				pt.put(c[1].as<std::string>() + ".total_initial_amount", 0);
				pt.put(c[1].as<std::string>() + ".total_net_amount", 0);
				pt.put(c[1].as<std::string>() + ".total_net_accrued_interest", 0);
				pt.put(c[1].as<std::string>() + ".previous_day_total_asset", 0);
											 //  ^ ga 0 harusnya masih ga tau ambil prev day nya
				pt.put(c[1].as<std::string>() + ".total_cash_today", c[6].as<long double>());
			}

			boost::gregorian::date today = boost::posix_time::microsec_clock::universal_time().date();

			// mengolah data
			result r = handler_.handle(worker_->conn, "SELECT * FROM POSITION;");
			for (result::const_iterator c = r.begin(); c != r.end(); ++c)
			{
				// delete yang uda lewat dari kemaren
				if((today - from_string(c[7].as<std::string>())).days() >= 1)
				{
					std::stringstream sql_delete;
					sql_delete << "DELETE FROM POSITION WHERE ID=" << c[0].as<int>() << ';';
					handler_.handle(worker_->conn, sql_delete.str());
				// sisanya di update nilainya
				} else
				{
					// hitung nilai baru
					long long new_accrued_interest = calculator_.calculate_interest(c[5].as<long double>(),
						c[8].as<long double>(), from_string(c[6].as<std::string>()),
						from_string(c[7].as<std::string>()), today);
					long long new_daily_accrued_interest = calculator_.calculate_interest(c[5].as<long double>(),
						c[8].as<long double>(), from_string(c[6].as<std::string>()),
						from_string(c[7].as<std::string>()), from_string(c[6].as<std::string>()) + days(1));
					long long new_initial_amount = c[5].as<long double>();

					// update nilai baru ke tabel position
					std::stringstream sql_update;
					sql_update << "UPDATE POSITION SET NET_ACCRUED_INTEREST = " << 
						new_accrued_interest <<	", DAILY_NET_ACCRUED_INTEREST = " <<
						new_daily_accrued_interest << "WHERE ID=" << c[0].as<int>() << ";";
					handler_.handle(worker_->conn, sql_update.str());

					ptree child = pt.get_child(c[1].as<std::string>()); // ptree dari product tertentu

					// jika hari ini cair, dipindahkan ke cash, dan amount_cair
					if((today - from_string(c[7].as<std::string>())).days() == 0)
					{
						// update tabel position
						std::stringstream sql;
						sql << "UPDATE POSITION SET WITHDRAWAL_MONEY = " << c[5].as<long double>() + new_accrued_interest <<
							", INITIAL_AMOUNT = 0, NET_ACCRUED_INTEREST = 0 WHERE ID = " << c[0].as<int>() << ";";
						handler_.handle(worker_->conn, sql.str());
						// total cash bertambah
						pt.put(c[1].as<std::string>() + ".total_cash_today", 
							child.get_child("total_cash_today").get_value<long long>() +
							new_initial_amount + new_accrued_interest);
						new_accrued_interest = 0; // sudah tidak ada di total deposito
						new_initial_amount = 0; // sudah tidak ada deposito di sana
					}

					// update data ptree sumarry
					pt.put(c[1].as<std::string>() + ".total_initial_amount",
						child.get_child("total_initial_amount").get_value<long long>() + new_initial_amount);
					pt.put(c[1].as<std::string>() + ".total_net_accrued_interest",
						child.get_child("total_net_accrued_interest").get_value<long long>() + new_accrued_interest);
					pt.put(c[1].as<std::string>() + ".total_net_amount",
						pt.get_child(c[1].as<std::string>() + ".total_initial_amount").get_value<long long>() +
						pt.get_child(c[1].as<std::string>() + ".total_net_accrued_interest").get_value<long long>());

					std::cout << child.get_child("total_net_amount").get_value<long long>() << std::endl;
				}
			}

			// update tabel summary
			for (result::const_iterator c = sum_res.begin(); c != sum_res.end(); ++c)
			{
				ptree child = pt.get_child(c[1].as<std::string>());
				handler_.handle(worker_->conn, "UPDATE SUMMARY SET TOTAL_INITIAL_AMOUNT = " +
					std::to_string(child.get_child("total_initial_amount").get_value<long long>()) +
					", TOTAL_NET_AMOUNT = " + std::to_string(child.get_child("total_net_amount")
					.get_value<long long>()) +", TOTAL_NET_ACCRUED_INTEREST = " +
					std::to_string(child.get_child("total_net_accrued_interest").get_value<long long>()) +
					", TOTAL_CASH_TODAY = " + std::to_string(child.get_child("total_cash_today")
					.get_value<long long>()) + ", TOTAL_DEPOSITO_TODAY = " +
					std::to_string(child.get_child("total_net_amount").get_value<long long>()) +
					"WHERE ID = " + std::to_string(c[0].as<int>()) + ";");
			}
		}
	} catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
	start_accept();
}