#include "../include/time_util.h"

std::string addZero(int in)
{
	return (in < 10 ? "0" : "") + std::to_string(in);
}

std::string getTime()
{
	boost::posix_time::ptime date_time = boost::posix_time::microsec_clock::universal_time();
	boost::posix_time::time_duration now = date_time.time_of_day();
	return to_simple_string(now).substr(0,8); // return as format hh:mm:ss
}