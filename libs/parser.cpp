#include "../include/parser.h"
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>

using namespace boost::log;
using namespace boost::gregorian;
using namespace boost::posix_time;

parser::parser()
{
	typedef sinks::asynchronous_sink<sinks::text_ostream_backend> text_sink;
	sink = boost::make_shared<text_sink>();

	boost::posix_time::ptime date_time = boost::posix_time::microsec_clock::universal_time();
	std::string date_now = std::to_string(date_time.date().year()) + '_' +
		addZero(date_time.date().month().as_number()) + '_' +
		addZero(date_time.date().day());
	log.open("../log/server_" + date_now + ".log", std::ios::out | std::ios::app );
	boost::shared_ptr<std::ostream> stream{&log, boost::null_deleter{}};
	sink->locked_backend()->add_stream(stream);
}

parser::~parser(){
}
void parser::parse(std::vector<message>* list_of_message, std::string input){

	std::vector<std::string> data;
	boost::split(data, input, [](char c){return c == '|' || c == '\n';});

	boost::posix_time::ptime time_now = time_from_string(data[1]);

	for(size_t i = 2; i + 5 < data.size(); i += 6)
	{
		try
		{
			message res(data[i], data[0], time_now, data[i+1],
				boost::lexical_cast<long long>(data[i+2]), from_string(data[i+3]),
				from_string(data[i+4]), boost::lexical_cast<long double>(data[i+5]), 0);

			list_of_message->push_back(res);
		} catch (std::exception& e)
		{
			BOOST_LOG(lg) << getTime() << '|' << "parser" << '|' << "1 >> " << "Id " << data[i] << " format is not accepted";
			sink->flush();

			std::cerr << e.what() << std::endl;
		}
	}
}