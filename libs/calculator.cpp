#include "../include/calculator.h"

using namespace boost::gregorian;

calculator::calculator()
{
}

calculator::~calculator()
{
}

long long calculator::calculate_interest(long long initial_amount, long double interest_rate,
	date start_date, date due_date, date today)
{
	long long days_passed = (today - start_date).days();
	long long days_total = (due_date - start_date).days();

	return initial_amount * interest_rate * net * days_passed /days_total;
}