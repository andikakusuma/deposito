#include "../../include/database/worker.h"

using namespace pqxx;

worker::worker(std::string str) : conn(str)
{
	try
	{
		std::cout << "Trying to connect to database" << std::endl;
		if(conn.is_open())
		{
			std::cout << "Success" << std::endl;
		}
	} catch (std::exception& e)
	{
		std::cerr << e.what() << std::endl;
	}
}

worker::~worker()
{
	conn.disconnect();
}