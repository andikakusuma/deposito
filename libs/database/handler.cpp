#include "../../include/database/handler.h"

using namespace pqxx;

handler::handler()
{
}
handler::~handler()
{
}
result handler::handle(connection& conn, std::string sql)
{
	work w(conn);
	result r(w.exec(sql));
	w.commit();
	return r;
}
